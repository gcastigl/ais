package com.gcastigl.ais.demo;

import com.gcastigl.ais.ga.GeneticAlgorithm;
import com.gcastigl.ais.ga.RandomGenerationSupplier;
import com.gcastigl.ais.ga.crossover.OnePointCrossover;
import com.gcastigl.ais.ga.mutation.UniformMutation;
import com.gcastigl.ais.ga.replacement.EliteReplacement;
import com.gcastigl.ais.ga.selection.RouletteSelection;
import com.gcastigl.ais.math.RandomDecimalSupplier;

public class GeneticAlgorithmDemo implements Runnable {

	public static void main(String[] args) {
		new GeneticAlgorithmDemo().run();
	}

	@Override
	public void run() {
		TestFunction function = new TestFunction();
		new GeneticAlgorithm<float[]>()
			.setCutCondition(g -> g.t() < 5000)
			.setInitialP(new RandomGenerationSupplier(6, 10, new RandomDecimalSupplier()).get())
			.setPhenotype(g -> {
				float x = Float.valueOf(g.alleles().substring(0, 5)) / 100000;
				float y = Float.valueOf(g.alleles().substring(5, 10))  / 100000;
				return new float[] {x, y};
			})
			.setFitness(p -> (float) Math.log(function.valueAt(p[0], p[1]) + 1f))
			.setSelection(new RouletteSelection())
			.setCrossover(new OnePointCrossover(), 0.9f)
			.setMutation(new UniformMutation(0.1f, new RandomDecimalSupplier()))
			.setReplacement(new EliteReplacement(0.5f))
			.run()
		;
	}

	private static final class TestFunction {

		private final float n = 9;
		private final float sigma = 0.15f;
		private final float sigmaSq = sigma * sigma;

		public float valueAt(float x, float y) {
			float dx = x - 0.5f;
			float dy = y - 0.5f;
			float rSq = dx * dx + dy * dy;
			float r = (float) Math.sqrt(rSq);
			float c1 = (float) Math.cos(n * Math.PI * r);
			float c2 = (float) Math.exp(-rSq / sigmaSq);
			return c1 * c1 * c2;
		}
	}
}
