package com.gcastigl.ais.demo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.gcastigl.ais.ann.MSE;
import com.gcastigl.ais.ann.NeuralNet;
import com.gcastigl.ais.ann.NeuralNetBuilder;
import com.gcastigl.ais.ann.function.SigmoidFunction;
import com.gcastigl.ais.ann.function.TransferFunction;
import com.gcastigl.ais.ga.Generation;
import com.gcastigl.ais.ga.GeneticAlgorithm;
import com.gcastigl.ais.ga.Genotype;
import com.gcastigl.ais.ga.crossover.OnePointCrossover;
import com.gcastigl.ais.ga.cutcondition.AfterGenerations;
import com.gcastigl.ais.ga.genotype.ANNGenotype;
import com.gcastigl.ais.ga.genotype.ANNMSEFitness;
import com.gcastigl.ais.ga.mutation.UniformMutation;
import com.gcastigl.ais.ga.replacement.EliteReplacement;
import com.gcastigl.ais.ga.selection.RouletteSelection;
import com.gcastigl.ais.math.RandomDecimalSupplier;

public class NeuralNetGeneticAlgorithmDemo implements Runnable {

	public static void main(String[] args) {
		new NeuralNetGeneticAlgorithmDemo().run();
	}

	private final ANNGenotype _nngenotype;
	private final List<Pair<float[], float[]>> _examples = new LinkedList<>();

	public NeuralNetGeneticAlgorithmDemo() {
		TransferFunction f = new SigmoidFunction();
		NeuralNet prototype = new NeuralNetBuilder()
			.withInputSize(2)
			.addLayer(2, f).addLayer(3, f).addLayer(2, f).get();
		_nngenotype = new ANNGenotype(prototype);
		_examples.add(Pair.of(new float[] {0, 0}, new float[] {0, 0}));
		_examples.add(Pair.of(new float[] {0, 1}, new float[] {0, 1}));
		_examples.add(Pair.of(new float[] {1, 0}, new float[] {1, 0}));
		_examples.add(Pair.of(new float[] {1, 1}, new float[] {1, 1}));
	}

	@Override
	public void run() {
		GeneticAlgorithm<NeuralNet> ga = new GeneticAlgorithm<NeuralNet>()
			.setPhenotype(g -> fromGenotype(g))
			.setInitialP(initialP(10))
			.setCutCondition(new AfterGenerations(5000))
			.setFitness(new ANNMSEFitness(_examples))
			.setSelection(new RouletteSelection())
			.setCrossover(new OnePointCrossover(), 0.9f)
			.setMutation(new UniformMutation(0.1f, new RandomDecimalSupplier()))
			.setReplacement(new EliteReplacement(0.5f));
		ga.run();
		NeuralNet net = ga.best();
		System.out.println("********** Best **********");
		System.out.println("MSE: " + new MSE().calc(net, _examples));
		_examples.forEach(e -> {
			System.out.println("Expected: " + Arrays.toString(e.getRight()));
			String output = Arrays.toString(net.evaluate(e.getLeft()).asVector());
			System.out.println("Output:" + output);
			System.out.println();
		});
		
	}

	private NeuralNet fromGenotype(Genotype g) {
		return _nngenotype.build(g);
	}

	private Generation initialP(int N) {
		Generation generation = new Generation(0, N);
		for (int i = 0; i < generation.N(); i++) {
			generation.addGenotype(_nngenotype.random());
		}
		return generation;
	}

}
