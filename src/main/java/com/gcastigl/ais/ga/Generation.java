package com.gcastigl.ais.ga;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;

public class Generation {

	private int _t;
	private int _N;
	private List<Genotype> _genotypes;
	private Stream<Genotype> _genotypesStream;

	public Generation(int N) {
		this(0, N);
	}

	public Generation(int t, int N) {
		_t = t;
		_N = N;
		_genotypes = new ArrayList<>();
		_genotypesStream = genotypes().stream();
	}

	public int t() {
		return _t;
	}

	public int N() {
		return _N;
	}

	public void addGenotypes(Collection<Genotype> gs) {
		genotypes().addAll(gs);
		Preconditions.checkState(genotypes().size() <= N());
	}

	public void addGenotype(Genotype g) {
		genotypes().add(g);
		Preconditions.checkState(genotypes().size() <= N());
	}

	public Genotype genotype(int i) {
		return genotypes().get(i);
	}

	public List<Genotype> genotypes() {
		return _genotypes;
	}

	public Stream<Genotype> genotypesStream() {
		return _genotypesStream;
	}

	public Generation next() {
		return new Generation(t() + 1, N());
	}

	@Override
	public String toString() {
		return "Gen: " + _t + " > \n\t" + StringUtils.join(genotypes(), "\n\t");
	}
}
