package com.gcastigl.ais.ga;

import com.google.common.base.Preconditions;

public class Genotype implements Comparable<Genotype> {

	private float _fitness;
	private StringBuilder _alleles;

	public Genotype(StringBuilder alleles) {
		_alleles = alleles;
	}

	public final void setFitness(float fitness) {
		Preconditions.checkArgument(Float.isFinite(fitness));
		_fitness = fitness;
	}

	public final float fitness() {
		return _fitness;
	}

	public final StringBuilder alleles() {
		return _alleles;
	}

	public void setAlleles(StringBuilder alleles) {
		_alleles = alleles;
	}

	public char allele(int i) {
		return alleles().charAt(i);
	}

	@Override
	public int compareTo(Genotype o) {
		return Float.compare(o.fitness(), fitness());
	}

	@Override
	public String toString() {
		return _fitness + " > " + alleles().toString();
	}
}
