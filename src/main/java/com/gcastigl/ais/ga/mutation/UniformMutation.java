package com.gcastigl.ais.ga.mutation;

import java.util.function.Supplier;

import com.gcastigl.ais.ga.Genotype;
import com.gcastigl.ais.math.Random2;
import com.google.common.base.Preconditions;

public class UniformMutation implements MutationMethod {

	private float _p;
	private Supplier<Character> _mutations;

	public UniformMutation(float p, Supplier<Character> mutations) {
		Preconditions.checkArgument(0 <= p && p <= 1);
		_p = p;
		_mutations = mutations;
	}

	@Override
	public void accept(Genotype t) {
		if (Math.random() < _p) {
			t.alleles().setCharAt(Random2.range(0, t.alleles().length()), _mutations.get());
		}
	}
}
