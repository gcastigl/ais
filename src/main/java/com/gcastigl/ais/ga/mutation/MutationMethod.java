package com.gcastigl.ais.ga.mutation;

import java.util.function.Consumer;

import com.gcastigl.ais.ga.Genotype;

public interface MutationMethod extends Consumer<Genotype> {

}
