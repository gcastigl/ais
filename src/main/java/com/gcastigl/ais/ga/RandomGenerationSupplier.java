package com.gcastigl.ais.ga;

import java.util.function.Supplier;

/**
 * This class is a supplier of randomly-generated Generations.
 *
 */
public class RandomGenerationSupplier implements Supplier<Generation> {

	private int _N;
	private int _allelesCount;
	private final Supplier<Character> _randomAlleles;

	/**
	 * @param N
	 *            Number of Genotype per generation
	 * @param allelesCount
	 *            Number of Alleles per Genotype
	 * @param randomAlleles
	 *            Supplier of alleles to use when generating an Allele
	 */
	public RandomGenerationSupplier(int N, int allelesCount, Supplier<Character> randomAlleles) {
		_N = N;
		_allelesCount = allelesCount;
		_randomAlleles = randomAlleles;
	}

	@Override
	public Generation get() {
		Generation generation = new Generation(0, _N);
		for (int i = 0; i < _N; i++) {
			StringBuilder alleles = new StringBuilder(_allelesCount);
			for (int j = 0; j < _allelesCount; j++) {
				alleles.append(_randomAlleles.get());
			}
			generation.addGenotype(new Genotype(alleles));
		}
		return generation;
	}
}
