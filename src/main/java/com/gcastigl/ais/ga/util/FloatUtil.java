package com.gcastigl.ais.ga.util;

public class FloatUtil {

	private static final int EXP = 1000;
	public static final int STR_LENGHT = 8;

	public static String serialize(float f) {
		int rounded = (int) (f * EXP);
		return String.format("%08d", rounded);
	}

	public static float deserialize(String s) {
		return Float.parseFloat(s) / EXP;
	}

}
