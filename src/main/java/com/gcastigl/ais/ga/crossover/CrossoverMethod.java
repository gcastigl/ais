package com.gcastigl.ais.ga.crossover;

import java.util.List;

import com.gcastigl.ais.ga.Genotype;

public interface CrossoverMethod {

	void apply(Genotype g1, Genotype g2, List<Genotype> result);

}
