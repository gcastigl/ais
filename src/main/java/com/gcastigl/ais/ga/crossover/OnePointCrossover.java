package com.gcastigl.ais.ga.crossover;

import java.util.List;

import com.gcastigl.ais.ga.Genotype;
import com.google.common.base.Preconditions;

public class OnePointCrossover implements CrossoverMethod {

	@Override
	public void apply(Genotype g1, Genotype g2, List<Genotype> result) {
		StringBuilder alleles1 = g1.alleles();
		StringBuilder alleles2 = g2.alleles();
		Preconditions.checkArgument(alleles1.length() == alleles2.length());
		int n = alleles1.length();
		int p = (int) (n * Math.random());
		result.add(cross(n, alleles1.subSequence(0, p), alleles2.subSequence(p, n)));
		result.add(cross(n, alleles2.subSequence(0, p), alleles1.subSequence(p, n)));
	}

	private Genotype cross(int n, CharSequence h1, CharSequence h2) {
		StringBuilder alleles = new StringBuilder(n);
		alleles.append(h1).append(h2);
		Preconditions.checkArgument(alleles.length() == n);
		return new Genotype(alleles);
	}
}
