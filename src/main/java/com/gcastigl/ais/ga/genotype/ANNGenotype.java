package com.gcastigl.ais.ga.genotype;

import com.gcastigl.ais.ann.NeuralNet;
import com.gcastigl.ais.ann.NeuralNetLayer;
import com.gcastigl.ais.ga.Genotype;
import com.gcastigl.ais.ga.util.FloatUtil;
import com.gcastigl.ais.math.Random2;
import com.gcastigl.ais.matrix.FloatMatrix;

public class ANNGenotype {

	private final NeuralNet _prototype;
	private int _floatsCount = 0;
	private int _allelesSize = 0;

	public ANNGenotype(NeuralNet prototype) {
		_prototype = prototype;
		_prototype.layers().forEach(layer -> {
			_floatsCount += layer.weights().totalSize();
			_floatsCount += layer.bias().totalSize();
		});
		_allelesSize = _floatsCount * FloatUtil.STR_LENGHT;
	}

	public int allelesSize() {
		return _allelesSize;
	}

	public Genotype random() {
		StringBuilder alleles = new StringBuilder(allelesSize());
		for (int i = 0; i < _floatsCount; i++) {
			alleles.append(FloatUtil.serialize(Random2.range(-5, 5)));
		}
		return new Genotype(alleles);
	}

	public NeuralNet build(Genotype g) {
		StringBuilder alleles = g.alleles();
		int index = 0;
		NeuralNet net = _prototype.copy();
		for (NeuralNetLayer layer : net.layers()) {
			FloatMatrix weights = layer.weights();
			float[][] values = weights.values();
			for (int row = 0; row < weights.rows(); row++) {
				for (int col = 0; col < weights.cols(); col++) {
					values[row][col] = FloatUtil.deserialize(alleles.substring(index, index + FloatUtil.STR_LENGHT));
					index += FloatUtil.STR_LENGHT;
				}
			}
			float[][] bias = layer.bias().values();
			for (int col = 0; col < bias[0].length; col++) {
				bias[0][col] = FloatUtil.deserialize(alleles.substring(index, index + FloatUtil.STR_LENGHT));
				index += FloatUtil.STR_LENGHT;
			}
		}
		return net;
	}

}
