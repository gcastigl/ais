package com.gcastigl.ais.ga.genotype;

import java.util.List;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;

import com.gcastigl.ais.ann.MSE;
import com.gcastigl.ais.ann.NeuralNet;

public class ANNMSEFitness implements Function<NeuralNet, Float> {

	private final MSE _mse = new MSE();
	private final List<Pair<float[], float[]>> examples;

	public ANNMSEFitness(List<Pair<float[], float[]>> examples) {
		this.examples = examples;
	}

	@Override
	public Float apply(NeuralNet t) {
		float mse = _mse.calc(t, examples);
		if (mse < 0.0001) {
			return 1000f;
		}
		return 1 / (mse * 1000);
	}

}
