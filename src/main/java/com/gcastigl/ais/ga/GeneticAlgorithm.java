package com.gcastigl.ais.ga;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import com.gcastigl.ais.ga.crossover.CrossoverMethod;
import com.gcastigl.ais.ga.mutation.MutationMethod;
import com.gcastigl.ais.ga.replacement.ReplacementMethod;
import com.gcastigl.ais.ga.selection.SelectionMethod;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

public class GeneticAlgorithm<T> {

	private Generation _intialP;
	private Predicate<Generation> _cutCondition;
	private Function<Genotype, T> _phenotype;
	private Function<T, Float> _fitness;
	private SelectionMethod _selection;
	private CrossoverMethod _crossover;
	private MutationMethod _mutate;
	private ReplacementMethod _replacement;

	private float _pc;
	private final Consumer<Genotype> _evaluate = (g -> g.setFitness(_fitness.apply(_phenotype.apply(g))));

	private T _best;

	public GeneticAlgorithm<T> setCutCondition(Predicate<Generation> cutCondition) {
		_cutCondition = cutCondition;
		return this;
	}

	public GeneticAlgorithm<T> setInitialP(Generation initialP) {
		_intialP = initialP;
		return this;
	}

	public GeneticAlgorithm<T> setPhenotype(Function<Genotype, T> phenotype) {
		_phenotype = phenotype;
		return this;
	}

	public GeneticAlgorithm<T> setFitness(Function<T, Float> fitness) {
		_fitness = fitness;
		return this;
	}

	public Function<T, Float> fitness() {
		return _fitness;
	}

	public GeneticAlgorithm<T> setSelection(SelectionMethod selection) {
		_selection = selection;
		return this;
	}

	public GeneticAlgorithm<T> setCrossover(CrossoverMethod crossover, float pc) {
		_crossover = crossover;
		_pc = pc;
		return this;
	}

	public GeneticAlgorithm<T> setMutation(MutationMethod mutation) {
		_mutate = mutation;
		return this;
	}

	public GeneticAlgorithm<T> setReplacement(ReplacementMethod replacement) {
		_replacement = replacement;
		return this;
	}

	public T best() {
		return _best;
	}

	public void run() {
		requireNonNull(_intialP);
		Generation p = _intialP;
		Genotype bestIndividual = null;
		while (_cutCondition.apply(p)) {
			evaluate(p);
			bestIndividual = findBest(p, bestIndividual);
			p = replacement(p, mutate(crossover(p, selection(p))));
		}
		_best = _phenotype.apply(bestIndividual);
	}

	protected void evaluate(Generation p) {
		p.genotypesStream().forEach(_evaluate);
	}

	protected List<Genotype> selection(Generation p) {
		return _selection.select(p.N(), p.genotypes());
	}

	protected List<Genotype> crossover(Generation p, List<Genotype> selected) {
		List<Genotype> newGenotypes = new ArrayList<>();
		for (List<Genotype> pair : Lists.partition(selected, 2)) {
			if (pair.size() == 2) {
				Genotype g1 = pair.get(0);
				Genotype g2 = pair.get(1);
				if (Math.random() < _pc) {
					_crossover.apply(g1, g2, newGenotypes);
				} else {
					newGenotypes.add(g1);
					newGenotypes.add(g2);
				}
			} else {
				newGenotypes.addAll(pair);
			}
		}
		return newGenotypes;
	}

	protected List<Genotype> mutate(List<Genotype> genotypes) {
		genotypes.stream().forEach(_mutate);
		return genotypes;
	}

	protected Generation replacement(Generation p, List<Genotype> newGenotypes) {
		Generation next = p.next();
		_replacement.replace(p, newGenotypes, next);
		return next;
	}

	private Genotype findBest(Generation p, Genotype best) {
		for (Genotype g : p.genotypes()) {
			if (best == null || best.fitness() < g.fitness()) {
				best = g;
			}
		}
		return best;
	}
}
