package com.gcastigl.ais.ga.cutcondition;

import com.gcastigl.ais.ga.Generation;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class AfterGenerations implements Predicate<Generation> {

	private int maxGenerations;

	public AfterGenerations(int maxGenerations) {
		Preconditions.checkArgument(maxGenerations > 0);
		this.maxGenerations = maxGenerations;
	}

	@Override
	public boolean apply(Generation input) {
		return input.t() < maxGenerations;
	}
}
