package com.gcastigl.ais.ga.replacement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gcastigl.ais.ga.Generation;
import com.gcastigl.ais.ga.Genotype;

public class EliteReplacement implements ReplacementMethod {

	private float _g;

	public EliteReplacement(float g) {
		_g = g;
	}

	@Override
	public float g() {
		return _g;
	}

	@Override
	public void replace(Generation p, List<Genotype> selected, Generation next) {
		List<Genotype> best = new ArrayList<Genotype>(p.genotypes());
		Collections.sort(best);
		next.addGenotypes(best.subList(0, n1(p.N())));
		next.addGenotypes(selected.subList(0, n2(p.N())));
	}

}
