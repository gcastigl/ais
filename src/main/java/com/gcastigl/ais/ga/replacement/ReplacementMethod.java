package com.gcastigl.ais.ga.replacement;

import java.util.List;

import com.gcastigl.ais.ga.Generation;
import com.gcastigl.ais.ga.Genotype;
import com.google.common.base.Preconditions;

public interface ReplacementMethod {

	/**
	 * <pre>
	 * G = 1, Whole original population is replaced.
	 * G = 0, No genotype from the original population is replaced
	 * </pre>
	 */
	float g();

	void replace(Generation p, List<Genotype> selected, Generation next);

	public default int n1(int N) {
		float g = g();
		Preconditions.checkArgument(0 <= g && g <= 1);
		return (int) (N * (1 - g));
	}

	public default int n2(int N) {
		float g = g();
		Preconditions.checkArgument(0 <= g && g <= 1);
		return (int) (N * g);
	}
}
