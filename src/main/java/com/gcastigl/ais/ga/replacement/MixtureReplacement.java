package com.gcastigl.ais.ga.replacement;

import java.util.List;

import com.gcastigl.ais.ga.Generation;
import com.gcastigl.ais.ga.Genotype;
import com.gcastigl.ais.ga.selection.SelectionMethod;

public class MixtureReplacement implements ReplacementMethod {

	private float _g;

	private SelectionMethod _m1, _m2;

	public MixtureReplacement(SelectionMethod m1, SelectionMethod m2, float g) {
		_m1 = m1;
		_m2 = m2;
		_g = g;
	}

	@Override
	public float g() {
		return _g;
	}

	@Override
	public void replace(Generation p, List<Genotype> selected, Generation next) {
		int N = p.N();
		next.addGenotypes(_m1.select(n1(N), p.genotypes()));
		next.addGenotypes(_m2.select(n2(N), selected));
	}
}
