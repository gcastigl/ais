package com.gcastigl.ais.ga.selection;

import static java.util.stream.IntStream.range;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.gcastigl.ais.ga.Genotype;

/**
 * A cada indivduo se le asigna una porción de la ruleta proporcional a su
 * fitness.
 */
public class RouletteSelection implements SelectionMethod {

	@Override
	public List<Genotype> select(int n, List<Genotype> genotypes) {
		int size = genotypes.size();
		Stream<Genotype> stream = genotypes.stream();
		float totalF = (float) stream.mapToDouble(g -> g.fitness()).sum();
		float[] pis = new float[size];
		range(0, size).forEach(i -> pis[i] = genotypes.get(i).fitness() / totalF);
		float[] qis = new float[size];
		range(0, size).forEach(i -> qis[i] = pis[i] + (i == 0 ? 0 : qis[i - 1]));
		qis[size - 1] = 1; // Avoid error because of rounding
		List<Genotype> selected = new ArrayList<>(n);
		IntStream.range(0, n).forEach(i -> {
			final float r = (float) Math.random();
			for (int j = 0; j < qis.length; j++) {
				if (r <= qis[j]) {
					selected.add(genotypes.get(j));
					break;
				}
			}
		});
		return selected;
	}

}
