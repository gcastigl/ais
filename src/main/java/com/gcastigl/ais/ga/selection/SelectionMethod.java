package com.gcastigl.ais.ga.selection;

import java.util.List;

import com.gcastigl.ais.ga.Genotype;

public interface SelectionMethod {

	List<Genotype> select(int n, List<Genotype> genotypes);

}
