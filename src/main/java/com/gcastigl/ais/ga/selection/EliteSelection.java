package com.gcastigl.ais.ga.selection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gcastigl.ais.ga.Genotype;

/**
 * <pre>
 * Se seleccionan los k individuos más aptos. 
 * Se los aparea para generar k descendientes. 
 * Se eligen los mejores k entre el total de 2k (padres + hijos), los cuales pasan a la siguiente generación
 * </pre>
 */
public class EliteSelection implements SelectionMethod {

	@Override
	public List<Genotype> select(int n, List<Genotype> genotypes) {
		List<Genotype> sorted = new ArrayList<>(genotypes);
		Collections.sort(sorted);
		return sorted.subList(0, n);
	}

}
