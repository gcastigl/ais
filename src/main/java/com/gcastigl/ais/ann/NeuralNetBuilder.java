package com.gcastigl.ais.ann;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;
import java.util.function.Supplier;

import com.gcastigl.ais.ann.function.TransferFunction;
import com.google.common.collect.Lists;

public class NeuralNetBuilder implements Supplier<NeuralNet> {

	private int _input;
	private final List<NeuralNetLayerData> _perceptrons = Lists.newArrayList();

	public NeuralNetBuilder withInputSize(int input) {
		_input = input;
		return this;
	}

	public NeuralNetBuilder addLayer(int perceptrons, TransferFunction f) {
		_perceptrons.add(new NeuralNetLayerData(perceptrons, f));
		return this;
	}

	@Override
	public NeuralNet get() {
		checkArgument(!_perceptrons.isEmpty());
		NeuralNet net = new NeuralNet();
		int inputSize = _input;
		for (int i = 0; i < _perceptrons.size(); i++) {
			NeuralNetLayerData layerDef = _perceptrons.get(i);
			NeuralNetLayer layer = new NeuralNetLayer(inputSize, layerDef.perceptrons, layerDef.f);
			net.add(layer);
			inputSize = layer.outputSize();
		}
		new RandomizeNet().apply(net);
		return net;
	}

	private static class NeuralNetLayerData {

		Integer perceptrons;
		TransferFunction f;

		public NeuralNetLayerData(Integer perceptrons, TransferFunction f) {
			this.perceptrons = perceptrons;
			this.f = f;
		}
	}
}
