package com.gcastigl.ais.ann;

import static java.util.Objects.requireNonNull;

import java.util.function.Function;

import com.gcastigl.ais.ann.function.TransferFunction;
import com.gcastigl.ais.matrix.FloatMatrix;

public class NeuralNetLayer {

	private final FloatMatrix _weights;
	private final FloatMatrix _bias;
	private final TransferFunction _transferF;

	public NeuralNetLayer(int input, int output, TransferFunction f) {
		_weights = new FloatMatrix(output, input);
		_bias = new FloatMatrix(1, output);
		_transferF = requireNonNull(f);
	}

	public final FloatMatrix weights() {
		return _weights;
	}

	public final float[] weights(int index) {
		return _weights.values()[index];
	}

	public final Function<Float, Float> f() {
		return _transferF.valfF();
	}

	public final Function<Float, Float> df() {
		return _transferF.dValfF();
	}

	public final int perceptronsCount() {
		return outputSize();
	}

	public final int outputSize() {
		return _weights.rows();
	}

	public final int inputSize() {
		return _weights.cols();
	}

	public FloatMatrix bias() {
		return _bias;
	}

	public NeuralNetLayer copy() {
		NeuralNetLayer copy = new NeuralNetLayer(inputSize(), outputSize(), _transferF);
		copy._weights.copyFrom(_weights);
		copy._bias.copyFrom(_bias);
		return copy;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("W:\n").append(weights()).append("Bias:\n").append(bias()).append("\n");
		return s.toString();
	}
}
