package com.gcastigl.ais.ann;

import org.apache.commons.lang3.RandomUtils;

import com.gcastigl.ais.matrix.FloatMatrix;
import com.google.common.base.Function;

public class RandomizeNet implements Function<NeuralNet, NeuralNet> {

	@Override
	public NeuralNet apply(NeuralNet net) {
		for (NeuralNetLayer layer : net.layers()) {
			randomizeValues(layer);
		}
		return net;
	}

	private NeuralNetLayer randomizeValues(NeuralNetLayer layer) {
		FloatMatrix weights = layer.weights();
		for (int i = 0; i < weights.rows(); i++) {
			for (int j = 0; j < weights.cols(); j++) {
				weights.values()[i][j] = RandomUtils.nextFloat(0, 2) - 1;
			}
		}
		float[] bias = layer.bias().asVector();
		for (int i = 0; i < layer.perceptronsCount(); i++) {
			bias[i] = RandomUtils.nextFloat(0, 2) - 1;
		}
		return layer;
	}
}
