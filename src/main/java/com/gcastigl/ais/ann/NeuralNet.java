package com.gcastigl.ais.ann;

import java.util.List;
import java.util.Optional;

import com.gcastigl.ais.matrix.FloatMatrix;
import com.google.common.collect.Lists;

public class NeuralNet {

	private final List<NeuralNetLayer> _layers = Lists.newArrayList();

	public final List<NeuralNetLayer> layers() {
		return _layers;
	}

	public final NeuralNetLayer layer(int index) {
		return _layers.get(index);
	}

	public final int layersCount() {
		return _layers.size();
	}

	public final NeuralNet add(NeuralNetLayer layer) {
		_layers.add(layer);
		return this;
	}

	public final int inputSize() {
		return layer(0).inputSize();
	}

	public final int outputSize() {
		return layer(layersCount() - 1).outputSize();
	}

	public FloatMatrix evaluate(float[] inputArray) {
		return evaluate(inputArray, Optional.empty(), Optional.empty());
	}

	public FloatMatrix evaluate(float[] inputArray, Optional<List<FloatMatrix>> es, Optional<List<FloatMatrix>> ys) {
		FloatMatrix input = new FloatMatrix(inputArray);
		for (NeuralNetLayer layer : layers()) {
			input = layer.weights().times(input).minusEquals(layer.bias().transpose());
			if (es.isPresent()) {
				es.get().add(input.clone());
			}
			input.applyEquals(layer.f());
			if (ys.isPresent()) {
				ys.get().add(input.clone());
			}
		}
		return input.transpose();
	}

	public NeuralNet copy() {
		NeuralNet copy = new NeuralNet();
		for (NeuralNetLayer layer : layers()) {
			copy.layers().add(layer.copy());
		}
		return copy;
	}

	public void setTo(NeuralNet other) {
		for (int i = 0; i < layers().size(); i++) {
			layer(i).weights().copyFrom(other.layer(i).weights());
			layer(i).bias().copyFrom(other.layer(i).bias());
		}
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder("{\n");
		int i = 0;
		for (NeuralNetLayer layer : layers()) {
			s.append("Layer:" + i++ + "\n");
			s.append(layer.toString() + "\n");
		}
		return s.append("}").toString();
	}
}
