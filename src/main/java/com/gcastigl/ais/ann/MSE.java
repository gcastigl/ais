package com.gcastigl.ais.ann;

import static com.gcastigl.ais.matrix.Vectors.norm;
import static com.gcastigl.ais.matrix.Vectors.sub;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class MSE {

	public float calc(NeuralNet net, List<Pair<float[], float[]>> examples) {
		float totalError = 0;
		for (Pair<float[], float[]> pair : examples) {
			float[] expectedOutput = pair.getRight();
			float[] y = net.evaluate(pair.getLeft()).asVector();
			totalError += mseError(y, expectedOutput);
		}
		totalError /= 2 * examples.size();
		return totalError;
	}

	private float mseError(float[] v1, float[] v2) {
		return norm(2, sub(v1, v2));
	}

}
