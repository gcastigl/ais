package com.gcastigl.ais.ann.function;

/**
 * "y" Values range between (0 and 1)
 */
public final class SigmoidFunction implements TransferFunction {

	private float _beta = 0.5f;

	public final SigmoidFunction withBeta(float beta) {
		_beta = beta;
		return this;
	}

	public final float beta() {
		return _beta;
	}

	@Override
	public float val(float h) {
		return 1 / (float) (1 + Math.pow(Math.E, -2 * _beta * h));
	}

	@Override
	public float dVal(float h) {
		float g = val(h);
		return 2 * beta() * g * (1 - g);
	}

}
