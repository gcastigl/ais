package com.gcastigl.ais.ann.function;

/**
 * "y" values range between (-1 and 1)
 */
public class HyperbolicFunction implements TransferFunction {

	private float _beta = 0.5f;

	public void setBeta(float beta) {
		_beta = beta;
	}

	@Override
	public float val(float h) {
		return (float) Math.tanh(_beta * h);
	}

	@Override
	public float dVal(float h) {
		float g = val(h);
		return _beta * (1 - g * g);
	}

}
