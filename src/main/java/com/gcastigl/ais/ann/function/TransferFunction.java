package com.gcastigl.ais.ann.function;

import java.util.function.Function;

public interface TransferFunction {

	float val(float h);

	default Function<Float, Float> valfF() {
		return (h) -> val(h);
	}

	float dVal(float h);

	default Function<Float, Float> dValfF() {
		return (h) -> dVal(h);
	}
}
