package com.gcastigl.ais.ann.train;

import static com.gcastigl.ais.matrix.Vectors.dot;
import static com.gcastigl.ais.matrix.Vectors.norm;
import static com.gcastigl.ais.matrix.Vectors.sub;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;

import com.gcastigl.ais.ann.NeuralNet;
import com.gcastigl.ais.ann.NeuralNetLayer;
import com.gcastigl.ais.matrix.FloatMatrix;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class Backpropagation {

	private List<Float> errors = Lists.newLinkedList();
	private List<Float> etas = Lists.newLinkedList();

	private NeuralNet _net;
	private float _eta = 0.02f;
	private float _epochsCount = 3000f;
	private Optional<Function<Float, Float>> _noise = Optional.empty();

	private boolean _adaptativeEta = false;
	private int _timesWithErrorDecrement = 0;
	private final float minEta = 0.01f;
	private final float maxEta = 0.3f;

	public Backpropagation(NeuralNet net) {
		_net = net;
	}

	public Backpropagation withEta(float eta) {
		_eta = eta;
		return this;
	}

	public Backpropagation withEpochsCount(float epochsCount) {
		_epochsCount = epochsCount;
		return this;
	}

	public Backpropagation withAdaptativeEta() {
		_adaptativeEta = true;
		return this;
	}

	public Backpropagation withNoise(float p, float amount) {
		_noise = Optional.of((f) -> {
			if (Math.random() < p) {
				float delta = (float) Math.random() - 0.5f;
				f += delta * amount * f;
			}
			return f;
		});
		return this;
	}

	public Backpropagation apply(List<Pair<float[], float[]>> training) {
		float previousError = 0;
		for (int i = 0; i < _epochsCount; i++) {
			NeuralNet prevNet = _adaptativeEta ? _net.copy() : null;
			float error = epoch(training);
			applyNoise();
			if (i != 0) {
				if (updateEta(previousError, error)) {
					_net.setTo(prevNet);
					prevNet = null;
				} else {
					etas.add(_eta);
					errors.add(error);
					previousError = error;
				}
			} else {
				previousError = error;
			}
		}
		return this;
	}

	private float epoch(List<Pair<float[], float[]>> training) {
		float totalError = 0;
		for (Pair<float[], float[]> pair : training) {
			List<FloatMatrix> es = Lists.newArrayList();
			List<FloatMatrix> ys = Lists.newArrayList();
			float[] y = _net.evaluate(pair.getLeft(), Optional.of(es), Optional.of(ys)).asVector();
			float[] expectedOutput = pair.getRight();
			totalError = mseError(y, expectedOutput);
			correct(new FloatMatrix(pair.getLeft()), es, ys, new FloatMatrix(expectedOutput));
		}
		totalError /= 2 * training.size();
		return totalError;
	}

	private float mseError(float[] v1, float[] v2) {
		return norm(2, sub(v1, v2));
	}

	private void correct(FloatMatrix input, List<FloatMatrix> es, List<FloatMatrix> ys, FloatMatrix expectedOutput) {
		List<FloatMatrix> deltas = new ArrayList<>(_net.layersCount());
		FloatMatrix delta;
		deltas.add(delta = expectedOutput.minusEquals(Iterables.getLast(ys)));
		for (int i = _net.layersCount() - 1; i > 0; i--) {
			deltas.add(delta = delta.times(_net.layer(i).weights()));
		}
		ys.add(0, input);
		Collections.reverse(deltas);
		for (int layerIndex = 0; layerIndex < _net.layersCount(); layerIndex++) {
			NeuralNetLayer layer = _net.layer(layerIndex);
			FloatMatrix deltaT = deltas.get(layerIndex);
			FloatMatrix de = es.get(layerIndex).transpose().applyEquals(layer.df());
			FloatMatrix ds = new FloatMatrix(dot(deltaT.asVector(), de.asVector())).timesEquals(_eta);
			FloatMatrix x = ys.get(layerIndex).transpose();
			FloatMatrix dw = ds.times(x);
			layer.weights().plusEquals(dw);
			// Bias
			layer.bias().minusEquals(ds.transpose());

		}
	}

	private void applyNoise() {
		if (_noise.isPresent()) {
			for (NeuralNetLayer layer : _net.layers()) {
				layer.weights().applyEquals(_noise.get());
				layer.bias().applyEquals(_noise.get());
			}
		}
	}

	private boolean updateEta(float previousError, float error) {
		if (_adaptativeEta) {
			System.out.println("ETA ADAPTATIVO NO ANDA BIEN!!");
			float deltaError = error - previousError;
			float deltaEta = 0;
			if (deltaError > 0) {
				deltaEta = -0.5f * _eta;
			} else {
				_timesWithErrorDecrement++;
				if (_timesWithErrorDecrement > 5) {
					deltaEta = 0.01f;
					_timesWithErrorDecrement = 0;
				}
			}
			_eta = Math.min(maxEta, Math.max(_eta + deltaEta, minEta));
			return deltaEta < 0;
		}
		return false;
	}

}
