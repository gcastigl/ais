package com.gcastigl.ais.matrix;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Arrays;
import java.util.function.Function;

public final class FloatMatrix {

	private float[][] _values;
	private int _rows, _cols;

	public FloatMatrix(int rows, int cols) {
		checkArgument(rows > 0);
		checkArgument(cols > 0);
		_values = new float[rows][cols];
		_rows = rows;
		_cols = cols;
	}

	public FloatMatrix(float[] values) {
		_rows = values.length;
		_cols = 1;
		_values = new float[values.length][];
		for (int i = 0; i < values.length; i++) {
			_values[i] = new float[] { values[i] };
		}
	}

	public final int rows() {
		return _rows;
	}

	public final int cols() {
		return _cols;
	}

	public final int totalSize() {
		return cols() * rows();
	}

	public final float[][] values() {
		return _values;
	}

	public final float value(int i, int j) {
		return _values[i][j];
	}

	public float[] asVector() {
		checkArgument(rows() == 1, "Not a vector");
		return _values[0];
	}

	public final FloatMatrix times(FloatMatrix B) {
		if (B.rows() != cols()) {
			throw new IllegalArgumentException("Matrix inner dimensions must agree.");
		}
		FloatMatrix X = new FloatMatrix(rows(), B.cols());
		int rowsInA = rows();
		int columnsInA = cols();
		int columnsInB = B.cols();
		float[][] c = X._values;
		for (int i = 0; i < rowsInA; i++) {
			for (int j = 0; j < columnsInB; j++) {
				for (int k = 0; k < columnsInA; k++) {
					c[i][j] = c[i][j] + _values[i][k] * B._values[k][j];
				}
			}
		}
		return X;
	}

	public final FloatMatrix plus(FloatMatrix B) {
		checkSameDimensions(B);
		FloatMatrix X = new FloatMatrix(rows(), cols());
		float[][] C = X.values();
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				C[i][j] = _values[i][j] + B._values[i][j];
			}
		}
		return X;
	}

	public final FloatMatrix transpose() {
		FloatMatrix X = new FloatMatrix(cols(), rows());
		float[][] C = X.values();
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				C[j][i] = _values[i][j];
			}
		}
		return X;
	}

	public final FloatMatrix plusEquals(FloatMatrix B) {
		checkSameDimensions(B);
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				_values[i][j] = _values[i][j] + B._values[i][j];
			}
		}
		return this;
	}

	public final FloatMatrix minusEquals(FloatMatrix B) {
		checkSameDimensions(B);
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				_values[i][j] = _values[i][j] - B._values[i][j];
			}
		}
		return this;
	}

	public FloatMatrix timesEquals(float s) {
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				_values[i][j] = s * _values[i][j];
			}
		}
		return this;
	}

	public FloatMatrix applyEquals(Function<Float, Float> f) {
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				_values[i][j] = f.apply(_values[i][j]);
			}
		}
		return this;
	}

	public final FloatMatrix clone() {
		FloatMatrix clone = new FloatMatrix(rows(), cols());
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				clone._values[i][j] = _values[i][j];
			}
		}
		return clone;
	}

	public final void copyFrom(FloatMatrix B) {
		checkSameDimensions(B);
		for (int i = 0; i < rows(); i++) {
			for (int j = 0; j < cols(); j++) {
				_values[i][j] = B.value(i, j);
			}
		}
	}

	private void checkSameDimensions(FloatMatrix B) {
		if (B.rows() != rows() || B.cols() != cols()) {
			throw new IllegalArgumentException("Matrix dimensions must agree.");
		}
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder(rows() + "x" + cols()).append("\n");
		for (float[] rows : values()) {
			s.append(Arrays.toString(rows)).append("\n");
		}
		return s.toString();
	}
}
