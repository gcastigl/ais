package com.gcastigl.ais.matrix;

import java.util.Iterator;
import java.util.List;

public class FloatMatrixs {

	public static final void flatten(FloatMatrix m, List<Float> list) {
		for (int i = 0; i < m.rows(); i++) {
			for (int j = 0; j < m.cols(); j++) {
				list.add(m.value(i, j));
			}
		}
	}

	public static final void load(FloatMatrix m, Iterator<Float> values) {
		for (int i = 0; i < m.rows(); i++) {
			for (int j = 0; j < m.cols(); j++) {
				m.values()[i][j] = values.next();
			}
		}
	}
}
