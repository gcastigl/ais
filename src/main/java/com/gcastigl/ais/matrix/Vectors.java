package com.gcastigl.ais.matrix;

import com.google.common.base.Preconditions;

public class Vectors {

	public static float[] dot(float[] v1, float[] v2) {
		Preconditions.checkArgument(v1.length == v2.length);
		return dot(v1, v2, new float[v1.length]);
	}

	public static float[] dot(float[] v1, float[] v2, float[] result) {
		Preconditions.checkArgument(v1.length == v2.length && result.length == v1.length);
		int len = v1.length;
		for (int i = 0; i < len; i++) {
			result[i] = v1[i] * v2[i];
		}
		return result;
	}

	public static float[] sub(float[] v1, float[] v2) {
		Preconditions.checkArgument(v1.length == v2.length);
		return sub(v1, v2, new float[v1.length]);
	}

	public static float[] sub(float[] v1, float[] v2, float[] result) {
		Preconditions.checkArgument(v1.length == v2.length && result.length == v1.length);
		int len = v1.length;
		for (int i = 0; i < len; i++) {
			result[i] = v1[i] - v2[i];
		}
		return result;
	}

	public static float sum(float[] v) {
		float sum = 0f;
		for (int i = 0; i < v.length; i++) {
			sum += v[i];
		}
		return sum;
	}

	public static float norm(int n, float[] v) {
		float norm = 0f;
		for (int i = 0; i < v.length; i++) {
			norm += Math.pow(v[i], n);
		}
		return (float) Math.pow(norm, 1 / (float) n);
	}

	public static void copy(float[] from, float[] into) {
		Preconditions.checkArgument(from.length == into.length);
		System.arraycopy(from, 0, into, 0, from.length);
	}

}
