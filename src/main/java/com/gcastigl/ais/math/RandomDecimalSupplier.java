package com.gcastigl.ais.math;

import java.util.function.Supplier;

public class RandomDecimalSupplier implements Supplier<Character> {

	private static final Character[] decimals = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	@Override
	public Character get() {
		return decimals[(int) (Math.random() * 10)];
	}

}
