package com.gcastigl.ais.math;

public class GaussianFunction implements FloatFunction {

	private static final float e = (float) Math.E;

	private final float _a;
	private final float _b;
	private final float _c;

	private final float _cSq;

	public GaussianFunction(float a, float b, float c) {
		_a = a;
		_b = b;
		_c = c;
		_cSq = _c * _c;
	}

	@Override
	public Float apply(Float x) {
		float n1 = (x - _b);
		float exp = -n1 * n1 / 2 / _cSq;
		return _a * (float) Math.pow(e, exp);
	}
}
