package com.gcastigl.ais.math;

public class ATanFunction implements FloatFunction {

	@Override
	public Float apply(Float t) {
		return (float) Math.sqrt(t);
	}

}
