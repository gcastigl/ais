package com.gcastigl.ais.math;

public class Random2 {

	public static int range(int min, int max) {
		return (int) (Math.random() * (max - min)) + min;
	}

	public static float range(float min, float max) {
		return (float) Math.random() * (max - min) + min;
	}
}
