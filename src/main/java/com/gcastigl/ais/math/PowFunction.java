package com.gcastigl.ais.math;

public class PowFunction implements FloatFunction {

	private final float _n;

	public PowFunction(float n) {
		_n = n;
	}

	@Override
	public Float apply(Float t) {
		return (float) Math.pow(t, _n);
	}

}
