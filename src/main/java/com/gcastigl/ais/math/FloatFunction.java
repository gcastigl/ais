package com.gcastigl.ais.math;

import java.util.function.Function;

public interface FloatFunction extends Function<Float, Float> {

}
