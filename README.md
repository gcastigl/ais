# Information
This is a free-to-use general purpose library for working with *Genetic algorithms* and *Feedforward neural networks*.

The project is still under development. Use it under your own risk.

# Build
run mvn clean install

# Usage
## Running Genetic Algorithms
0. Define how will an individual going to be encoded into a Genotype
1. Create an instance of GeneticAlgorithm and configure:
    - Cut condition (setCutCondition)
    - Initial Population (setInitialP)
    - How to transform a Genotype into a Phenotype (setPhenotype)
    - Fitness Function (setFitness)
    - Selection Function (setSelection)
    - Crossover Method (setCrossover)
    - Mutation Method (setMutation)
    - Replacement Method (setReplacement)
2. Execute run()
3. Get best Individual using best()

### Selection Functions
- Elite
- Roulette

Other methods may be implemented using interface *SelectionMethod*

### Crossover Methods
- OnePoint

  Other methods may be implemented using interface *CrossoverMethod*

### Mutation Methods
- Uniform

Other methods may be implemented using interface *MutationMethod*

### Replacement Methods
- Elite
- Mixture

Other methods may be implemented using interface *ReplacementMethod*

## Neural Networks
### Building
1. Build an instance of *NeuralNetBuilder*
2. Define Input layer (withInputSize)
3. Define rest of layers (addLayer)
    - Each layer can have defined its own TransferFunction. The same instance may be used for all of them if desired.
4. Build using get()

### Backpropagation trainging
1. Build an instance of *Backpropagation* and configure:
    - Eta (withEta)
    - Epochs (withEpochsCount)
    - Optionally, adaptative eta usage can be set (withAdaptativeEta)
    - Optionally, noise may be added to weight values (withNoise)
2. Execute apply and provide a testing set and a training set